package audio.voxware.com.audiotest;

import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.content.LocalBroadcastManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by edh on 10/29/2015.
 */
public class TTSPlayer {
    public static final String ACTION_SPEAK_START = "com.voxware.browser.android.action.SPEAK_START";
    public static final String ACTION_SPEAK_STOP = "com.voxware.browser.android.action.SPEAK_STOP";
    public static final String ACTION_SPEAK_ERROR = "com.voxware.browser.android.action.SPEAK_ERROR";
    public static final String EXTRA_TTS_TEXT = "com.voxware.browser.android.extra.TTS_TEXT";
    public static final String EXTRA_TTS_ERROR = "com.voxware.browser.android.extra.TTS_ERROR";

    private static final Logger log = LoggerFactory.getLogger(TTSPlayer.class);

    private final TextToSpeech textToSpeech;
    private final Context context;
    private Map<String, TtsTask> utterances = new LinkedHashMap<>();
    private final BlockingQueue<Runnable> taskQueue = new ArrayBlockingQueue<Runnable>(1);
    private ExecutorService executorService = new ThreadPoolExecutor(1, 1, 10, TimeUnit.MINUTES, taskQueue, new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, "TtsAdapterThread");
        }
    });

    private final AtomicInteger count = new AtomicInteger(0);

    private CountDownLatch latch = new CountDownLatch(1);

    private float pitch = 1.0f;
    private float speechRate = 1.0f;
    private float volume = 1.0f;

    public TTSPlayer(Context context) {
        this.context = context;
        this.textToSpeech = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                latch.countDown();
            }
        });
    }

    public synchronized Future<Void> play(String output) throws Exception {
        String id = Integer.toString(count.getAndIncrement());
        log.info("Queuing \"" + output + "\" for playback");
        final TtsRequest req = new TtsRequest(id, output, this.pitch, this.speechRate, this.volume);
        TtsTask task = new TtsTask(req);
        utterances.put(id, task);
        try {
            executorService.submit(task);
        } catch (RejectedExecutionException e) {
            throw new Exception(e);
        }
        return task;
    }

    protected class TtsRequest implements Callable<Void> {
        private final String id;
        private final String utterance;
        private final float pitch;
        private final float speechRate;
        private final float volume;
        private final CountDownLatch latch = new CountDownLatch(1);
        private final AtomicInteger status = new AtomicInteger(0);
        private final UtteranceProgressListener utteranceProgressListener = new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {
                TtsRequest.this.onStart(utteranceId);
            }

            @Override
            public void onDone(String utteranceId) {
                TtsRequest.this.onDone(utteranceId);
            }

            @Override
            public void onError(String utteranceId) {
                TtsRequest.this.onError(utteranceId);
            }
        };

        public TtsRequest(final String id, final String utterance, final float pitch, final float speechRate, final float volume) {
            this.id = id;
            this.utterance = utterance.trim().toLowerCase();
            this.pitch = pitch;
            this.speechRate = speechRate;
            this.volume = volume;
        }

        public Void call() throws Exception {
            // sending an empty string to TextToSpeech has weird behavior (no callbacks get invoked, seems to hang)
            if (utterance == null || utterance.isEmpty()) {
                onStart(id);
                onDone(id);
                return null;
            }
            try {
                log.info("Queuing \"" + utterance + "\" for playback");
                //textToSpeech.speak(output, TextToSpeech.QUEUE_FLUSH, Bundle.EMPTY, output);
                HashMap<String, String> args = new HashMap<String, String>();
                args.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, id);
                args.put(TextToSpeech.Engine.KEY_PARAM_VOLUME, Float.toString(volume));
                int result = textToSpeech.setOnUtteranceProgressListener(utteranceProgressListener);
                if (result != TextToSpeech.SUCCESS) {
                    throw new Exception("Failed to register listener: " + result);
                }
                result = textToSpeech.setPitch(pitch);
                if (result != TextToSpeech.SUCCESS) {
                    throw new Exception("Failed to set pitch: " + result);
                }
                result = textToSpeech.setSpeechRate(speechRate);
                if (result != TextToSpeech.SUCCESS) {
                    throw new Exception("Failed to set rate: " + result);
                }
                result = textToSpeech.speak(utterance, TextToSpeech.QUEUE_ADD, args);
                if (result != TextToSpeech.SUCCESS) {
                    throw new Exception("Failed to queue \"" + utterance + "\": " + result);
                }
                // now we wait till we are notified of completion, or we get interrupted (cancelled)
                latch.await();
                if (status.intValue() == 0) {
                    return null;
                } else {
                    throw new Exception("Error: " + status.intValue());
                }
            } catch (InterruptedException e) {
                log.info("Playback of \"" + utterance + "\" cancelled");
                textToSpeech.stop();
                throw new CancellationException("Playback of \"" + utterance + "\" cancelled");
            }
        }

        public void onStart(String utteranceId) {
            log.debug("Started playing " + utteranceId + "[" + utterance + "]");
            Intent intent = new Intent();
            intent.setAction(ACTION_SPEAK_START);
            intent.putExtra(EXTRA_TTS_TEXT, utterance);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }

        public void onDone(String utteranceId) {
            log.debug("Finished playing " + utteranceId + "[" + utterance + "]");
            Intent intent = new Intent();
            intent.setAction(ACTION_SPEAK_STOP);
            intent.putExtra(EXTRA_TTS_TEXT, utterance);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            latch.countDown();
        }

        public void onError(String utteranceId) {
            // we use a generic error
            onError(utteranceId, -1);
        }

        public void onError(String utteranceId, int errorCode) {
            Intent intent = new Intent();
            intent.setAction(ACTION_SPEAK_ERROR);
            intent.putExtra(EXTRA_TTS_TEXT, utterance);
            intent.putExtra(EXTRA_TTS_ERROR, errorCode);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            status.set(errorCode);
            latch.countDown();
        }
    }

    protected class TtsTask extends FutureTask<Void> {
        private final TtsRequest request;
        public TtsTask(TtsRequest request) {
            super(request);
            this.request = request;
        }

        public TtsRequest getTtsRequest() {
            return request;
        }

        protected void done() {
            synchronized (TTSPlayer.this) {
                utterances.remove(request.id);
            }
        }
    }
}
