package audio.voxware.com.audiotest;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.CancellationException;

/**
 * Created by eric on 4/12/16.
 */
public class AudioLoop {


        private static final Logger log = LoggerFactory.getLogger(AudioLoop.class);
        private TTSPlayer tts;
        private boolean loopRunning = false;
        private LoopThread loop = new LoopThread();
        private Thread thread;
        private final short [] shorts;
        private int frequency = 11025;
        private Context context;


        public AudioLoop(Context context, byte[] content) {
            this.shorts = new short[content.length / 2];
            ByteBuffer.wrap(content).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
            this.context = context;
            this.tts = new TTSPlayer(context);
        }

        public synchronized void start() {
            if (!loopRunning) {
                loop = (new LoopThread());
                thread = new Thread(loop);
                thread.start();
                loopRunning = true;
            }
        }

        public synchronized void stop() {
            if (loopRunning) {
                loopRunning = false;
                thread.interrupt();
            }
        }

        private final void playAudio() {
            AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, frequency, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT,
                        shorts.length * 2, AudioTrack.MODE_STATIC);

            try {
                int writeResult = audioTrack.write(shorts, 0, shorts.length);
                if (writeResult != shorts.length) {
                    String errorMsg = null;
                    if (writeResult < 0) {
                        errorMsg = String.format("Error writing shorts to audio track: %d", writeResult);
                    } else {
                        errorMsg = String.format("Failed to write full buffer to audio track %d<%d", writeResult, shorts.length);
                    }
                    log.error(errorMsg);
                    throw new Exception(errorMsg);
                }
                log.info("Rendering audio samples");
                audioTrack.play();
                waitForCompletion(audioTrack);
            } catch (Exception e) {
                // we must have been cancelled?
                throw new CancellationException();
            } finally {
                log.info("Rendering complete");
                audioTrack.stop();
                audioTrack.release();
            }
        }

        private final void playTTS() {
            try {
                tts.play("This is a red zebra");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public class LoopThread implements Runnable {

            @Override
            public void run() {
                while (loopRunning == true) {
                    try {
                        playTTS();
                        Thread.sleep(6000);
                        playAudio();
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        log.info("interrupted");
                    }
                }
            }
        }



        protected void waitForCompletion(AudioTrack audioTrack) throws InterruptedException {
            final long now = System.currentTimeMillis();
            final long estimatedEnd = now + calculateLengthInMillis(shorts.length);
            int oldCurrentPosition = 0;
            int currentPosition;

            synchronized(this) {
                currentPosition = audioTrack.getPlaybackHeadPosition();
            }

            int noFramesCount = 0;
            while (!Thread.currentThread().isInterrupted() && audioTrack.getPlayState() == AudioTrack.PLAYSTATE_PLAYING && currentPosition < shorts.length) {
                if (oldCurrentPosition == currentPosition) {
                    if (System.currentTimeMillis() <= estimatedEnd) {
                        noFramesCount++;
                    } else {
                        log.warn("Playback is stalled, frame counter hasn't moved " + noFramesCount + " times");
                        return;
                    }
                }
                oldCurrentPosition = currentPosition;
                currentPosition = audioTrack.getPlaybackHeadPosition();
                int framesRemaining = shorts.length - currentPosition;
                // divide by framesPerMillis, add 1ms means waiting just a little longer
                int millisLeft = calculateLengthInMillis(framesRemaining);
                // precision is an issue, hence the loop
                Thread.sleep(millisLeft);
            }
        }
        private int calculateLengthInMillis(int frames) {
            return ((frames * 1000) / frequency) + (frames % frequency == 0 ? 0 : 1);
        }

    }

